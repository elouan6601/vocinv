/*
Main file containing the main loop of the program
*/
import os {input}
fn main(){
	mut voc := Vocabulary{} //Struct d'interface avec les données
	voc.vocdict = {
		'dog':'chien'
		'cat':'chat'
		'bird':'oiseau'
		}
	voc.create_lists()
	voc.create_dict("test.yml")!
	//voc.create_dict("test.yml")!
	mut answered := false 
	for answered != true {
		listquery := input("Use native, foreign or global list? ")
		match listquery {
			"native" {voc.choosenvoclist = voc.nativevoclist; answered = true}
			"foreign" {voc.choosenvoclist = voc.foreignvoclist; answered = true}
			"global" {voc.choosenvoclist = voc.globalvoclist; answered = true}
			else {println("Please choose a valid list")}
		}
	}
	mut running := true
	maxscore := voc.choosenvoclist.len //Maximum score posible (number of words)
	mut score := 0 //Actual score
	mut wordsremain := 1 //Number of remaining words
	for running{
		word := voc.get_word()
		if word.len == 2{ //If words remain
			print("\n") 
			println("${wordsremain}/${maxscore}")
			println(word[0]) //display word in one language
			query := input("> ") //ask to translate
			if query == word[1]{
				score ++
				println("correct")
			} else if query == "exit"{
				running = false
			} else{
				println("false")
			}
			wordsremain ++
		} else {
			running = false
		}
	}
	println("\nscore: ${score}/${maxscore}")

}


