# VocInV
The goal is furnish an interface to learn vocabulary by displaying a word in a foreign language or user's native language and asking user to translate it. The program will be written in the [V lang](https://vlang.io/), I've choosen it for its rapidity of execution comparable to C and its syntax simplicity comparable to Python. \
I'm programming this software mostly for learning V
## Project Plan
Here are the steps I will follow in order: 
- [x] Make program functionnal in CLI and V's maps
- [ ] Make vocml functionnal
- [ ] Adding the possibility to use ~~yaml~~ **vocml** files as vocabulary lists
- [ ] Program a GUI with the VUI module
- [ ] Allow user to create yaml vocabulary lists via GUI
## Vocml
Vocml is a simple markup langage where file can contain only a single dictionnary, I chose to develop it because the only vpm's [yaml library](https://vpm.vlang.io/packages/prantlf.yaml) is way too complicated to use for a usage simple as storing dictionnaries, and I didn't wanted to develop a whole yaml library just for this usage.
## Instructions to compile
First you need to download and install [V](https://github.com/vlang/v/blob/master/README.md#installing-v-from-source) (I recommend you to build it from source), then compile the project by using this command from the project's directory `v .`
## Legal notice
**Author**: Elouan Deschamps \
**Licence**: [Gplv3](LICENSE) \
**Logo**: Project's emblem have been made with [Gnome emblem](https://apps.gnome.org/en-GB/Emblem/) and this [free of charge icon](https://www.reshot.com/free-svg-icons/item/learning-YZX7TM2FNB/)