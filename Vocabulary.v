/*
The file that contain the struct wich is used as an interface with vocabulary lists and yamls
*/
import prantlf.yaml as py
struct Vocabulary {
	mut:
		vocdict map[string]string //dictionnary of vocabulary
		nativevoclist [][]string //natives words indexed first
		foreignvoclist [][]string //foreigns word indexed first
		globalvoclist [][]string //both lists merged
		choosenvoclist [][]string //list choosen by user (native, foreign or global)
}

fn (Y Vocabulary) create_dict(path string) !{
	newdict := py.unmarshal_file[To_String](path)!
	print(newdict)
}

fn (Y Vocabulary) get_lists() string {
	//Return a string with all the content of the vocdict
	mut lists := ""
	for key, value in Y.vocdict {
		lists +='\n${key}:${value}'
	}
	return lists
}

fn (mut Y Vocabulary) create_lists() {
	//Fill the voclists, doesn't return anything
	for key, value in Y.vocdict {
		Y.nativevoclist << [key, value]
		Y.foreignvoclist << [value, key]
	}
	for word in Y.nativevoclist {
		Y.globalvoclist << word
	}
	for word in Y.foreignvoclist {
		Y.globalvoclist << word
	}
}

fn (mut Y Vocabulary) get_word() []string {
	//Return a len 2 tab with the word and pop it from the list, return a len 1 tab else
	mut returnedword := ["Vide"]
	if Y.choosenvoclist.len >= 1{ //if the list is not empty
		returnedword = Y.choosenvoclist.pop()
	}
	return returnedword
}